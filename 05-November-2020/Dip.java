public class Dip{
    public static void main(String[] args) {
//High-level modules should not depend on low-level modules.
// Both should depend on abstractions.

    }
}

public interface soda{
    void add();
}

public class Cocacola implements soda{

}

public class Pepsi implements soda{

}

class order{
    order(soda sodaOrdered)
    {
        this.soda=sodaOrdered;
    }
}