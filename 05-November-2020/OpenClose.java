import java.util.*;

//A class should have a single functionality
public class OpenClose{
    public static void main(String[] args)
    {
        Operation add= new addition();
        add.execute();
    }
}

public interface Operation{
    void execute();
}

//This way with out changing existing class/model
//we can add additional functionality
// (New operation can be added by implementing the operation interface)
class addition implements Operation
{
    int firstOperand;
    int secondOperand;

    addition(int value1,int value2)
    {
        this.firstOperand=value1;
        this.secondOperand=value2;
    }
    void execute()
    {
        System.out.println(firstOperand+secondOperand);
    }
}