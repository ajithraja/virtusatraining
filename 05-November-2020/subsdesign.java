//referenced from stackoverflow and https://springframework.guru/principles-of-object-oriented-design/liskov-substitution-principle/

class TrasportationDevice
{
   String name;
   String getName() { ... }
   void setName(String n) { ... }
   double speed;
   double getSpeed() { ... }
   void setSpeed(double d) { ... }
}

class DevicesWithoutEngines extends TransportationDevice
{  
   void startMoving() { ... }
}

class DevicesWithEngines extends TransportationDevice
{  
   Engine engine;
   Engine getEngine() { ... }
   void setEngine(Engine e) { ... }
   void startEngine() { ... }
}

class Car extends DevicesWithEngines
{
   @Override
   void startEngine() { ... }
}

class Bicycle extends DevicesWithoutEngines
{
   @Override
   void startMoving() { ... }
}
 






public class Bird{

}
public class FlyingBirds extends Bird{
    public void fly(){}
}
public class Duck extends FlyingBirds{}
public class Ostrich extends Bird{} 