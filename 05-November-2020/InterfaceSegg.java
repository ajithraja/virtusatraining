public class InterfaceSegg{
    public static void main(String[] args) {
        //no class or client should be forced to implement a method that the class or client is not using

    }

    public interface a{
    //common interface
    }
    public interface f{
    //logic specific to class C that is not needed in class B
    }
    class B implements a{

    }

    class C implements a,f{

    }
}