import java.util.*;

//A class should have a single functionality (A class to edit user data)
public class SingleRes{
    public static void main(String[] args)
    {
        public void changeAddress(User user)
        {
            if(Authorization.checkEditAccess(user))
            {

            }
        }
    }
}

//In different file ( A class to just authorize)
class Authorization
{
    public static boolean checkEditAccess(User user)
    {

    }
}