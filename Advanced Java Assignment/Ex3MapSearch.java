package com.java.assignment;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Ex3MapSearch {
    public static void main(String[] args) {
        Map<String,String> map=new HashMap<>();
        map.put("one","1");
        map.put("two","2");
        map.put("three","3");
        map.put("four","4");
        findValue(map,"one");


    }

    public static void findValue(Map<String, String> map,  String searchKey) {
        Optional<Map.Entry<String, String>> count= (map.entrySet().stream().filter(a -> a.getKey().equals(searchKey))).findAny();
        if(count.isPresent())
            System.out.println("Key found:"+searchKey);
        else
            System.out.println("NOT_FOUND");
    }
}
