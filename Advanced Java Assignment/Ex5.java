package com.java.assignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Student {
    String id;
    Map<Subject, Integer> report;

    public Student(String id, Map<Subject, Integer> report) {
        this.id = id;
        this.report = report;
    }

    @Override
    public String toString() {
        return "Student{" + "id='" + id + '}';
    }
}

enum Subject {     SCIENCE, MATHEMATICS, LANGUAGE, HISTORY }


public class Ex5 {

    static int maxmarkScience=0, maxMarkHistory =0, maxMarkLanguage =0, maxMarkMath =0;
    static Map<Subject,List<Student>> maxMarkStudents =new HashMap<>();

    public static void main(String[] args) {
        Map<Subject,Integer> reportMap=new HashMap<>();
        List<Student> studentList=new ArrayList<>();

        reportMap.put((Subject.SCIENCE),78);
        reportMap.put((Subject.LANGUAGE),91);
        reportMap.put((Subject.MATHEMATICS),95);
        reportMap.put((Subject.HISTORY),88);

        Student s1=new Student("1",reportMap);

        Map<Subject,Integer> reportMap2=new HashMap<>();
        reportMap2.put((Subject.SCIENCE),85);
        reportMap2.put((Subject.LANGUAGE),82);
        reportMap2.put((Subject.MATHEMATICS),95);
        reportMap2.put((Subject.HISTORY),91);

        Student s2=new Student("2",reportMap2);

        studentList.add(s1);
        studentList.add(s2);

        getBestScores(studentList);
        System.out.println(maxMarkStudents);
    }
    public static void getBestScores(List<Student> students) {

        List<Student> studentList=new ArrayList<>();
        maxMarkStudents.put(Subject.SCIENCE,studentList);
        maxMarkStudents.put(Subject.HISTORY,studentList);
        maxMarkStudents.put(Subject.LANGUAGE,studentList);
        maxMarkStudents.put(Subject.MATHEMATICS,studentList);

        students.stream().forEach(student->{
            maxmarkScience =maxMark(student,Subject.SCIENCE, maxmarkScience);
            maxMarkHistory =maxMark(student,Subject.HISTORY, maxMarkHistory);
            maxMarkLanguage =maxMark(student,Subject.LANGUAGE, maxMarkLanguage);
            maxMarkMath =maxMark(student,Subject.MATHEMATICS, maxMarkMath);
        });


    }
    public static int maxMark(Student currStudent,Subject sub,int currmax)
    {
        if(currStudent.report.get(sub)>currmax)
        {
            currmax=currStudent.report.get(sub);
            List<Student> newStudentList=new ArrayList<>();
            newStudentList.add(currStudent);
            maxMarkStudents.put(sub,newStudentList);

        }
        else if(currStudent.report.get(sub)==currmax)
        {
            List<Student> existingStudentList= maxMarkStudents.get(sub);
            existingStudentList.add(currStudent);
            maxMarkStudents.put(sub,existingStudentList);
        }
        return currmax;
    }

}

