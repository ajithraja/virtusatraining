package com.java.assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

import static java.util.Arrays.asList;

public class EX1AvgOfElements {

    public static void main(String[] args) {
        List<Integer> list =asList(2,4,55,7,77,45);
       OptionalDouble avg= list.stream().mapToInt(Integer::intValue).average();

        System.out.println("Avg of all the elements in the list is "+(int)avg.getAsDouble());

    }


}
