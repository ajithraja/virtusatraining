package com.java.assignment;

import java.util.HashMap;
import java.util.Map;

public class Ex2MapPrintAll {
    public static void main(String[] args) {
        Map<String,String> map=new HashMap<>();
        map.put("one","1");
        map.put("two","2");
        map.put("three","3");
        map.put("four","4");
        printMapKeyValue(map);
    }

    public static void printMapKeyValue(Map<String, String> map){
        map.forEach((a,b)-> System.out.println("Key:"+a+"=Value:"+b));
    }
}
