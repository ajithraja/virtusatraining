package com.java.assignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Ex4MapEqKey {
    public static void main(String[] args) {
        List<String> list=new ArrayList<String>();
        list.add("one");
        list.add("Ran");
        list.add("text");
        list.add("is");
        list.add("give");
        list.add("to");

        Map<Integer,List<String>> map=new HashMap<>();

        Map<Integer, List<String>> groupMap= new HashMap<>(
                list.stream().collect(Collectors.groupingBy(String::length))
        );

        System.out.println("Final HashMap \n"+groupMap);
    }
}
