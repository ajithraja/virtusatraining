package com.java.assignment;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Ex6ReadFromFile {
    public static void main(String[] args) {
        List<String> data=new ArrayList<>();
        try (Stream<Path> walk = Files.walk(Paths.get("C:\\Users\\ajbalaraman\\IdeaProjects\\advance-java-assignment\\src\\files"))) {


            walk.filter(Files::isRegularFile).forEach(x->{

                Stream<String>lines= null;
                try {
                    lines = Files.lines(x);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                lines.forEach(z-> data.add(z));

            });
            Files.write(Paths.get("file6.txt"), data,
                    StandardCharsets.UTF_8);

        } catch (IOException e) {
            e.printStackTrace();
        }
//
//        try {
//            File dir = new File("C:\\Users\\ajbalaraman\\IdeaProjects\\advance-java-assignment\\src\\files");
//            File newFile = new File("ResultFile.txt");
//
//            newFile.createNewFile();
//            FileWriter myWriter = new FileWriter("ResultFile.txt");
//           for(File myObj:dir.listFiles())
//            {
//                Scanner myReader = new Scanner(myObj);
//                while (myReader.hasNextLine()) {
//                    String data = myReader.nextLine();
//                    myWriter.write(data.toUpperCase());
//                    myWriter.write("\n");
//                }
//                myReader.close();
//            }
//           myWriter.close();
//        } catch (Exception e) {
//            System.out.println("File not found");
//
//        }
    }
}
