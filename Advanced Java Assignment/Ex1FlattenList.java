package com.java.assignment;

import java.util.*;
import static java.util.Arrays.asList;

public class Ex1FlattenList {

    public static void main(String[] args) {
        List<List<String>> nestedList = asList(
                asList("one:one"),
                asList("two:one", "two:two", "two:three"),
                asList("three:one", "three:two", "three:three", "three:four"));
        System.out.println("Given nested list: \n\t"+nestedList);
        System.out.println("Flattened List: \n\t"+getFlattenList(nestedList));
    }

    //Given method
    public static List<String> getFlattenList(List<List<String>> listOfListOfString){
        List<String> ls = new ArrayList<>();
        listOfListOfString.forEach(ls::addAll);
        return ls;
    }


}
