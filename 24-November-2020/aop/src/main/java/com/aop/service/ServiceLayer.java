package com.aop.service;

import com.aop.repo.Addition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceLayer {
    @Autowired
    Addition addition;
    public void addition()
    {
        System.out.println(addition.add());
    }
}
