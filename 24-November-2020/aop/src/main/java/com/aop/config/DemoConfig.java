package com.aop.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Aspect
@Configuration
public class DemoConfig {

    @Before("execution(public void addition())")
    public void beforeAdd()
    {
        System.out.println("Before Addition");
    }

    @After("execution(public void addition())")
    public void afterAdd()
    {
        System.out.println("After Addition");
    }

    @Around("execution(public void addition())")
    public void aroundAdd(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("Around Before Addition");
        pjp.proceed();
        System.out.println("Around After Addition");
    }

}
