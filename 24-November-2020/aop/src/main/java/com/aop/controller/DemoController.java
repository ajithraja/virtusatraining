package com.aop.controller;

import com.aop.service.ServiceLayer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
    @Autowired
    ServiceLayer serviceLayer;

    @GetMapping("/test")
    public void test()
    {
        serviceLayer.addition();
    }
}
